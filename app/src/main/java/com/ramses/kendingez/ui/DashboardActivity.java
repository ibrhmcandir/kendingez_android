package com.ramses.kendingez.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.ramses.kendingez.R;

public class DashboardActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
    }
}