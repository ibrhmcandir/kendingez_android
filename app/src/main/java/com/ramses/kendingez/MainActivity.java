package com.ramses.kendingez;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.ramses.kendingez.ui.DashboardActivity;
import com.ramses.kendingez.ui.OnboardingActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences preferences = getSharedPreferences("KENDIN_GEZ_PREF", MODE_PRIVATE);
        boolean isFirst = preferences.getBoolean("isFirst", true);

        if(isFirst){
            startActivity(new Intent(MainActivity.this, OnboardingActivity.class));
        }
        else{
            startActivity(new Intent(MainActivity.this, DashboardActivity.class));
        }

    }
}